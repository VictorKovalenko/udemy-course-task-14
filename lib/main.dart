import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:udemy_course_task14/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyProvider(),
      child: MaterialApp(
        title: 'Udemy course task 14',
        theme: ThemeData.dark(),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 100.0,
              height: 50.0,
              decoration: BoxDecoration(),
              child: Material(
                color: Colors.cyan[400],
                borderRadius: BorderRadius.circular(18.0),
                child: InkWell(
                  child: Center(
                    child: Text(
                      '. . .',
                      style: TextStyle(color: Colors.black45),
                    ),
                  ),
                  borderRadius: BorderRadius.circular(18.0),
                  onTap: () {
                    context.read<MyProvider>().nameTwo();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Consumer<MyProvider>(
                          builder: (context, value, _) {
                            return Text(value.title);
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
